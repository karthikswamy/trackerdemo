import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  Platform,
} from "react-native";
import { geoLocation, getCurrentLocation, trackLocation, grant } from "./utils";
import * as TaskManager from "expo-task-manager";
import * as Permissions from "expo-permissions";
import * as Location from "expo-location";

const LOCATION_TRACKING = "background-location-task";

export default function App() {
  const [location, setLocation] = useState({
    lat: "",
    long: "",
    street: "",
    district: "",
    city: "",
    subregion: "",
    region: "",
    time: "",
  });
  const [route, setRoute] = useState([
    {
      lat: "",
      long: "",
      street: "",
      district: "",
      city: "",
      subregion: "",
      region: "",
      time: "",
    },
  ]);
  let Time = new Date(Date.now()).toLocaleString();



  function writeFile(input) {
    var RNFS = require("react-native-fs");
    var path = Platform.OS == 'ios' ? RNFS.DocumentDirectoryPath + '/location.txt' : RNFS.ExternalDirectoryPath + "/location.txt"
    // write the file
    //let path2 == Platform.OS ? path1 : path
    if (path == null) {
      RNFS.writeFile(path, input, "utf8")
        .then((success) => {
          console.log("FILE WRITTEN! at", path);
        })
        .catch((err) => {
          console.log(err.message);
        });
      RNFS.readFile(path, "utf8").then((contents) => {
        console.log("file content", contents);
      });
    } else {
      RNFS.appendFile(path, input, "utf8");
    }
  }

  async function backgroundTracking() {
    let { status } = await Location.requestPermissionsAsync();
    console.log("status", status);
    if (status !== "granted") {
      return;
    } else {
      status = "granted";
    }  
    let permissions = await Permissions.askAsync(Permissions.LOCATION);

    if (permissions.status !== "granted") {
      console.log("location denied");
      return;
    }

    try {
      await Location.startLocationUpdatesAsync(LOCATION_TRACKING, {
        accuracy: Location.Accuracy.BestForNavigation,
        showsBackgroundLocationIndicator: true,
        timeout: 10000,
        distanceInterval: 1, //meters
        timeInterval: 60000, //millisec

        activityType: Location.ActivityType.AutomotiveNavigation,
        foregroundService: {
          notificationTitle: "Alert",
          notificationBody: "Running on background",
          notificationColor: "#RRGGBB",
        },
      });
    } catch (e) {
      console.log("exception in background");
    }
    const hasStarted = await Location.hasStartedLocationUpdatesAsync(
      LOCATION_TRACKING
    );
  }

  TaskManager.defineTask(LOCATION_TRACKING, async ({ data, error }) => {
    if (error) {
      console.log("LOCATION_TRACKING task ERROR:", error);
      return;
    }
    if (data) {
      const { locations } = data;
      let lat = locations[0].coords.latitude;
      let long = locations[0].coords.longitude;
      let loc = { latitude: lat, longitude: long, time: Time };

      console.log(
        "background locations",
        `${loc.time}: ${loc.latitude},${loc.longitude}`
      );

      route.push({
        lat: lat,
        long: long,
        time: Time,
      });
      setRoute([...route]);
      writeFile(
        `latitude : ${loc.latitude}, longitude ${loc.longitude}, time: ${loc.time}\n`
      );
    }
  });

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <TouchableOpacity
        style={styles.button}
        onPress={() =>
          getCurrentLocation((position) => {
            setLocation({
              lat: position.latitude,
              long: position.longitude,
            });
            console.log("current address", position);
          })
        }
      >
        <Text style={styles.title}>current location</Text>
      </TouchableOpacity>
      <Text style={styles.content}>
        {location.lat} {location.long} {location.street} {location.district}{" "}
        {location.subregion} {location.region}
      </Text>
      <TouchableOpacity
        style={styles.button}
        onPress={() => backgroundTracking()}
      >
        <Text style={styles.title}>track</Text>
      </TouchableOpacity>
      <FlatList
        data={route}
        keyExtractor={(item, index) => index.toString()}
        style={{ alignContent: "center" }}
        renderItem={({ item }) => {
          return (
            <View style={styles.cell}>
              <Text style={styles.itemcontent}>
                {item.item}
                {item.lat} {item.long} {item.street} {item.district} {item.city}
                {item.subregion} {item.time}
              </Text>
            </View>
          );
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  button: {
    height: 40,
    backgroundColor: "blue",
    marginTop: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    marginHorizontal: 30,
  },
  title: {
    color: "white",
  },
  content: {
    color: "blue",
    textAlign: "center",
    marginTop: 10,
    marginHorizontal: 30,
  },
  cell: {
    padding: 10,
    paddingLeft: 30,
    width: "90%",
  },
  itemcontent: {
    color: "blue",
    fontSize: 15,
  },
});

