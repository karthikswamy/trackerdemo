import React, { Component, useState } from "react";
import {} from "react-native";
import * as Location from "expo-location";
import moment from "moment";

const LOCATION_TRACKING = "location-tracking";

var status = "error";
var backgroundPermission = "error";


export async function getCurrentLocation(callback) {
    let { status } = await Location.requestPermissionsAsync();
    console.log("status", status);
    if (status !== "granted") {
      return;
    } else {
      status = "granted";
    }  
    callback(status);

  let position = await Location.getCurrentPositionAsync(
    // accuracy : 3 - accurate to 100 m, 4 - accurate to 10m, 	5 - best accuracy, 6 - highest with more sensors
    { accuracy: 5, timeout: 1000, maximumAge: 1000 }
  );
  console.log("raw position", position);
  let locationObj = {
    ...position.coords,
    time: new Date(),
  };

  callback(locationObj);
  console.log("new location", locationObj);
}

//foreground tracking
export async function trackLocation(callback) {

  let { status } = await Location.requestPermissionsAsync();
  console.log("status", status);
  if (status !== "granted") {
    return;
  } else {
    status = "granted";
  }  
  
  callback(status);
  await Location.watchPositionAsync(
    {
      accuracy: 5,
      distanceInterval: 1, //meters
      timeInterval: 5000, //millisec
    },
    (newLocation) => {
      let loc = { ...newLocation.coords, time: moment().format("llll") };
      // geoLocation(loc, (res) => {
      //   callback(res);
      // });
      callback(loc)
    }
  );
}

//for getting address
export async function geoLocation(loc, callback) {
  if (loc == null) {
    callback("");
    return;
  }
  try {
    let result = await Location.reverseGeocodeAsync({
      latitude: loc.latitude,
      longitude: loc.longitude,
    });
    let address = result[0];
    console.log("loc", loc);
    let temp = {
      latitude: loc.latitude,
      longitude: loc.longitude,
      street: address.street,
      district: address.district,
      city: address.city,
      subregion: address.subregion,
      region: address.region,
      time: loc.time,
    };
    console.log("geo", temp);
    callback(temp);
  } catch (e) {
    console.log("exception geolocation");
  }
}